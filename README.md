# RR32-ETH01

# What is it?
This will be a board similar to the Wireless Tag WT32-ETH01, but with a more 'powerful' ESP32.
This board will be used in my own hardware designs, maybe also sold separately, but if you want then you can use the designs and build your own.

I am waiting for the Espressif ESP32-P4 to be released, before continuing this project.